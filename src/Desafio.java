
public class Desafio {
	private static final int HASH = 7;
	private static final int FACTOR = 37;
	private static String LETTERS = "acdegilmnoprstuw";
	private static final long TARGET = 695527946727L;
	
	public static void main(String[] args) {
		getString(TARGET);		
	}
	
	private static long getHash(String s) {
		long h = HASH;
		int lLen = s.length();
		for (int i = 0; i < lLen; i++) {
			h = h * FACTOR + LETTERS.indexOf(s.charAt(i));
		}
		return h;
	}
	
	public static String getString(long h){
		char[] perm = new char[HASH];
		permutation(perm, 0 ,LETTERS);
		return "";
	}
	
	private static void permutation(char[] perm, int pos, String str){
		if (pos == perm.length) {
	        if(getHash(String.valueOf(perm))==TARGET){
	        	System.out.println("Solução: "+new String(perm));
	        }
	    } else {
	        for (int i = 0 ; i < str.length() ; i++) {
	            perm[pos] = str.charAt(i);
	            permutation(perm, pos+1, str);
	        }
	    }
	}
}
